CREATE TABLE `product` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(128) NOT NULL,
	`description` TEXT NOT NULL,
	`price` decimal(10,2) unsigned DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `product_name` (`name`)
) ENGINE = MyISAM;

CREATE TABLE `client` (
	`id` INT(11) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(100) NOT NULL,
	`email` varchar(64) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `client_name` (`name`)
) ENGINE = MyISAM;

CREATE TABLE `order` (
	`id` smallint( 11 ) NOT NULL AUTO_INCREMENT ,
	`client_id` INT( 11 ) NOT NULL ,
	`created` datetime NOT NULL,
	`ip` VARCHAR(15) NOT NULL,
	`client_phone` INT(15) NOT NULL,
	PRIMARY KEY (`id`),
	INDEX `order_cliend_id` (`client_id`),
	INDEX `order_created` (`created`)
) ENGINE = InnoDB;

CREATE TABLE `order_product` (
	`order_id` smallint( 11 ) NOT NULL,
	`product_id` smallint( 11 ) NOT NULL,
	`amount` INT(5) NOT NULL,
	INDEX `order_product_order_id_fk` (`order_id` ASC),
	INDEX `order_product_product_id` (`product_id` ASC),
	CONSTRAINT `order_product_order_id_fk`
	FOREIGN KEY (`order_id`)
	REFERENCES `order` (`id`) 
	ON DELETE CASCADE
) ENGINE = InnoDB;



SELECT o.id,
       o.created,
       (
          SELECT SUM(op1.amount)
          FROM order_product op1
          WHERE op1.order_id = o.id
       ) AS count_product,
       (
          SELECT AVG(p1.price)
          FROM order_product op2
          LEFT JOIN product p1 ON op2.product_id = p1.id
          WHERE op2.order_id = o.id
            AND op2.product_id = p1.id
       ) AS avg_product_price
FROM `order` o
  LEFT JOIN order_product op on o.id = op.order_id
  LEFT JOIN product p on op.product_id = p.id
GROUP BY o.id
HAVING count_product > 1
ORDER BY o.created
LIMIT 10;
