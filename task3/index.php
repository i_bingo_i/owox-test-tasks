<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);

require_once 'CostStrategy/TimedCostStrategy.php';
require_once 'CostStrategy/FixedCostStrategy.php';
require_once 'CostStrategy/ExtraCostStrategy.php';
require_once 'Lessons/Speaking.php';
require_once 'Lessons/Grammar.php';
require_once 'Languages/EnglishLanguage.php';

$lessons = [];
$lessons[] = new Speaking(5, new TimedCostStrategy());
$lessons[] = new Grammar(4, new FixedCostStrategy());
$lessons[] = new EnglishLanguage(new Grammar(4, new FixedCostStrategy()), new ExtraCostStrategy());

/** @var Lesson $lesson */
foreach ($lessons as $lesson) {
    echo "Charge for lesson {$lesson->cost()}.";
    echo "Charge type {$lesson->chargeType()}.";
}
