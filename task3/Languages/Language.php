<?php

class Language
{
    /** @var Lesson */
    private $lesson;
    /** @var CostStrategy */
    private $costStrategy;

    /**
     * Language constructor.
     * @param Lesson $lesson
     * @param CostStrategy $costStrategy
     */
    public function __construct(Lesson $lesson, CostStrategy $costStrategy)
    {
        $this->lesson = $lesson;
        $this->costStrategy = $costStrategy;
    }

    /**
     * @return integer
     */
    public function cost()
    {
        return $this->costStrategy->cost($this->lesson);
    }

    /**
     * @return string
     */
    public function chargeType()
    {
        return $this->costStrategy->chargeType();
    }
}