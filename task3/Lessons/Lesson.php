<?php

class Lesson
{
    /** @var integer */
    private $duration;
    /** @var CostStrategy */
    private $costStrategy;

    /**
     * Lesson constructor.
     * @param $duration
     * @param CostStrategy $costStrategy
     */
    public function __construct($duration, CostStrategy $costStrategy)
    {
        $this->duration = $duration;
        $this->costStrategy = $costStrategy;
    }

    /**
     * @return integer
     */
    public function cost()
    {
        return $this->costStrategy->cost($this);
    }

    /**
     * @return string
     */
    public function chargeType()
    {
        return $this->costStrategy->chargeType();
    }

    /**
     * @return integer
     */
    public function getDuration()
    {
        return $this->duration;
    }
}