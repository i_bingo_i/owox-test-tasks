<?php

require_once 'CostStrategy.php';

class ExtraCostStrategy extends CostStrategy
{

    /**
     * @param Lesson $lesson
     * @return integer
     */
    function cost(Lesson $lesson)
    {
        return $lesson->cost() * 0.02 + $lesson->cost();
    }

    /**
     * @return string
     */
    function chargeType()
    {
        return "Extra cost";
    }
}