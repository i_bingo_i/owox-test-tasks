<?php

require_once 'CostStrategy.php';

class TimedCostStrategy extends CostStrategy
{
   /**
     * @param Lesson $lesson
     * @return integer
     */
    function cost(Lesson $lesson)
    {
        return $lesson->getDuration() * 100;
    }

    /**
     * @return string
     */
    function chargeType()
    {
        return "Hourly payment";
    }
}