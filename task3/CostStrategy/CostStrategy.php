<?php

abstract class CostStrategy
{
    /**
     * @param Lesson $lesson
     * @return integer
     */
    abstract function cost(Lesson $lesson);

    /**
     * @return string
     */
    abstract function chargeType();
}