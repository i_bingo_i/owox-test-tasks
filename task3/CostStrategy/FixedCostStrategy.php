<?php

require_once 'CostStrategy.php';

class FixedCostStrategy extends CostStrategy
{
    /**
     * @param Lesson $lesson
     * @return integer
     */
    function cost(Lesson $lesson)
    {
        return 200;
    }

    /**
     * @return string
     */
    function chargeType()
    {
        return "Fixed type";
    }
}