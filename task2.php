<?php

function luckyTickets($k) {
    $firsArray = [];
    $result = 0;
    
    $nextArray = function($array) {
        $arrayLength = count($array) + 9;
        $resNextArray = [];

        for ($i = 0; $i < $arrayLength; $i++) {
            $q = 0;

            for ($j = 0; $j < 10; $j++) {

                if (isset($array[$i - $j])) {
                    $q += $array[$i - $j];
                }

                $resNextArray[$i] = $q;
            }
        }
	
	    return $resNextArray;
    };
    
    for ($h = 0; $h < 10; $h++) {
	    array_push($firsArray, 1);
    }
    
    for ($a = 0; $a < ($k / 2 - 1); $a++) {
	    $firsArray = $nextArray($firsArray);
    }
    
    foreach($firsArray as $v) {
	    $result += pow($v, 2);
    }
    
    return $result;
}

