<?php

require_once '../task2.php';

class Task2Test extends PHPUnit_Framework_TestCase
{
    public function testAdd()
    {
        $result = luckyTickets(6);
        $this->assertEquals(55252, $result);
    }
}